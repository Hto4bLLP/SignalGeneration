# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 12.2.0 for Linux x86 (64-bit) (December 12, 2020)
# Date: Fri 2 Jul 2021 12:25:57



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
fa = Parameter(name = 'fa',
               nature = 'external',
               type = 'real',
               value = 1000000,
               texname = 'f_a',
               lhablock = 'ALPPARS',
               lhacode = [ 1 ])

CWtil = Parameter(name = 'CWtil',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'c_{\\tilde{W}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 2 ])

CBtil = Parameter(name = 'CBtil',
                  nature = 'external',
                  type = 'real',
                  value = 0.,
                  texname = 'c_{\\tilde{B}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 3 ])

cuR11 = Parameter(name = 'cuR11',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,11)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 4 ])

cuR12 = Parameter(name = 'cuR12',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,12)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 5 ])

cuR13 = Parameter(name = 'cuR13',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,13)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 6 ])

cuR21 = Parameter(name = 'cuR21',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,21)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 7 ])

cuR22 = Parameter(name = 'cuR22',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,22)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 8 ])

cuR23 = Parameter(name = 'cuR23',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,23)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 9 ])

cuR31 = Parameter(name = 'cuR31',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,31)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 10 ])

cuR32 = Parameter(name = 'cuR32',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,32)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 11 ])

cuR33 = Parameter(name = 'cuR33',
                  nature = 'external',
                  type = 'real',
                  value = 1.,
                  texname = 'c_{u_{\\text{Subsript}(R,33)}}',
                  lhablock = 'ALPPARS',
                  lhacode = [ 12 ])

cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Malp = Parameter(name = 'Malp',
                 nature = 'external',
                 type = 'real',
                 value = 50,
                 texname = '\\text{Malp}',
                 lhablock = 'MASS',
                 lhacode = [ 9000005 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WALP = Parameter(name = 'WALP',
                 nature = 'external',
                 type = 'real',
                 value = 1,
                 texname = '\\text{WALP}',
                 lhablock = 'DECAY',
                 lhacode = [ 9000005 ])

cuR1x1 = Parameter(name = 'cuR1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR11',
                   texname = '\\text{cuR1x1}')

cuR1x2 = Parameter(name = 'cuR1x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR12',
                   texname = '\\text{cuR1x2}')

cuR1x3 = Parameter(name = 'cuR1x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR13',
                   texname = '\\text{cuR1x3}')

cuR2x1 = Parameter(name = 'cuR2x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR21',
                   texname = '\\text{cuR2x1}')

cuR2x2 = Parameter(name = 'cuR2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR22',
                   texname = '\\text{cuR2x2}')

cuR2x3 = Parameter(name = 'cuR2x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR23',
                   texname = '\\text{cuR2x3}')

cuR3x1 = Parameter(name = 'cuR3x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR31',
                   texname = '\\text{cuR3x1}')

cuR3x2 = Parameter(name = 'cuR3x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR32',
                   texname = '\\text{cuR3x2}')

cuR3x3 = Parameter(name = 'cuR3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'cuR33',
                   texname = '\\text{cuR3x3}')

CGtil = Parameter(name = 'CGtil',
                  nature = 'internal',
                  type = 'real',
                  value = '(aS*cuR11*cmath.sqrt(1 + (277*aS)/(12.*cmath.pi)))/(8.*cmath.pi)',
                  texname = 'c_{\\tilde{G}}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.sin(cabi)',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '-cmath.sin(cabi)',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = 'cmath.cos(cabi)',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '1',
                   texname = '\\text{CKM3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

cdL11 = Parameter(name = 'cdL11',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x1**2*cuR22*ymc**2 + CKM2x1*CKM3x1*cuR31*ymc*ymt + CKM2x1*CKM3x1*cuR32*ymc*ymt + CKM3x1**2*cuR33*ymt**2 + CKM1x1*CKM2x1*cuR12*ymc*ymup + CKM1x1*CKM2x1*cuR21*ymc*ymup + CKM1x1*CKM3x1*cuR13*ymt*ymup + CKM1x1*CKM3x1*cuR31*ymt*ymup + CKM1x1**2*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,11)}}')

cdL12 = Parameter(name = 'cdL12',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x1*CKM2x2*cuR22*ymc**2 + CKM2x1*CKM3x2*cuR31*ymc*ymt + CKM2x2*CKM3x1*cuR32*ymc*ymt + CKM3x1*CKM3x2*cuR33*ymt**2 + CKM1x1*CKM2x2*cuR12*ymc*ymup + CKM1x2*CKM2x1*cuR21*ymc*ymup + CKM1x1*CKM3x2*cuR13*ymt*ymup + CKM1x2*CKM3x1*cuR31*ymt*ymup + CKM1x1*CKM1x2*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,12)}}')

cdL13 = Parameter(name = 'cdL13',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x1*CKM2x3*cuR22*ymc**2 + CKM2x1*CKM3x3*cuR31*ymc*ymt + CKM2x3*CKM3x1*cuR32*ymc*ymt + CKM3x1*CKM3x3*cuR33*ymt**2 + CKM1x1*CKM2x3*cuR12*ymc*ymup + CKM1x3*CKM2x1*cuR21*ymc*ymup + CKM1x1*CKM3x3*cuR13*ymt*ymup + CKM1x3*CKM3x1*cuR31*ymt*ymup + CKM1x1*CKM1x3*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,13)}}')

cdL21 = Parameter(name = 'cdL21',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x1*CKM2x2*cuR22*ymc**2 + CKM2x2*CKM3x1*cuR31*ymc*ymt + CKM2x1*CKM3x2*cuR32*ymc*ymt + CKM3x1*CKM3x2*cuR33*ymt**2 + CKM1x2*CKM2x1*cuR12*ymc*ymup + CKM1x1*CKM2x2*cuR21*ymc*ymup + CKM1x2*CKM3x1*cuR13*ymt*ymup + CKM1x1*CKM3x2*cuR31*ymt*ymup + CKM1x1*CKM1x2*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,21)}}')

cdL22 = Parameter(name = 'cdL22',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x2**2*cuR22*ymc**2 + CKM2x2*CKM3x2*cuR31*ymc*ymt + CKM2x2*CKM3x2*cuR32*ymc*ymt + CKM3x2**2*cuR33*ymt**2 + CKM1x2*CKM2x2*cuR12*ymc*ymup + CKM1x2*CKM2x2*cuR21*ymc*ymup + CKM1x2*CKM3x2*cuR13*ymt*ymup + CKM1x2*CKM3x2*cuR31*ymt*ymup + CKM1x2**2*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,22)}}')

cdL23 = Parameter(name = 'cdL23',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x2*CKM2x3*cuR22*ymc**2 + CKM2x2*CKM3x3*cuR31*ymc*ymt + CKM2x3*CKM3x2*cuR32*ymc*ymt + CKM3x2*CKM3x3*cuR33*ymt**2 + CKM1x2*CKM2x3*cuR12*ymc*ymup + CKM1x3*CKM2x2*cuR21*ymc*ymup + CKM1x2*CKM3x3*cuR13*ymt*ymup + CKM1x3*CKM3x2*cuR31*ymt*ymup + CKM1x2*CKM1x3*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,23)}}')

cdL31 = Parameter(name = 'cdL31',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x1*CKM2x3*cuR22*ymc**2 + CKM2x3*CKM3x1*cuR31*ymc*ymt + CKM2x1*CKM3x3*cuR32*ymc*ymt + CKM3x1*CKM3x3*cuR33*ymt**2 + CKM1x3*CKM2x1*cuR12*ymc*ymup + CKM1x1*CKM2x3*cuR21*ymc*ymup + CKM1x3*CKM3x1*cuR13*ymt*ymup + CKM1x1*CKM3x3*cuR31*ymt*ymup + CKM1x1*CKM1x3*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,31)}}')

cdL32 = Parameter(name = 'cdL32',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x2*CKM2x3*cuR22*ymc**2 + CKM2x3*CKM3x2*cuR31*ymc*ymt + CKM2x2*CKM3x3*cuR32*ymc*ymt + CKM3x2*CKM3x3*cuR33*ymt**2 + CKM1x3*CKM2x2*cuR12*ymc*ymup + CKM1x2*CKM2x3*cuR21*ymc*ymup + CKM1x3*CKM3x2*cuR13*ymt*ymup + CKM1x2*CKM3x3*cuR31*ymt*ymup + CKM1x2*CKM1x3*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,32)}}')

cdL33 = Parameter(name = 'cdL33',
                  nature = 'internal',
                  type = 'real',
                  value = '((CKM2x3**2*cuR22*ymc**2 + CKM2x3*CKM3x3*cuR31*ymc*ymt + CKM2x3*CKM3x3*cuR32*ymc*ymt + CKM3x3**2*cuR33*ymt**2 + CKM1x3*CKM2x3*cuR12*ymc*ymup + CKM1x3*CKM2x3*cuR21*ymc*ymup + CKM1x3*CKM3x3*cuR13*ymt*ymup + CKM1x3*CKM3x3*cuR31*ymt*ymup + CKM1x3**2*cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(16.*cmath.pi**2*vev**2)',
                  texname = 'c_{d_{\\text{Subsript}(L,33)}}')

cH = Parameter(name = 'cH',
               nature = 'internal',
               type = 'real',
               value = '(3*(cuR22*ymc**2 + cuR33*ymt**2 + cuR11*ymup**2)*cmath.log(fa**2/ymt**2))/(8.*cmath.pi**2*vev**2)',
               texname = '\\text{c$\\_$H}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

cdL1x1 = Parameter(name = 'cdL1x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL11',
                   texname = '\\text{cdL1x1}')

cdL1x2 = Parameter(name = 'cdL1x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL12',
                   texname = '\\text{cdL1x2}')

cdL1x3 = Parameter(name = 'cdL1x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL13',
                   texname = '\\text{cdL1x3}')

cdL2x1 = Parameter(name = 'cdL2x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL21',
                   texname = '\\text{cdL2x1}')

cdL2x2 = Parameter(name = 'cdL2x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL22',
                   texname = '\\text{cdL2x2}')

cdL2x3 = Parameter(name = 'cdL2x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL23',
                   texname = '\\text{cdL2x3}')

cdL3x1 = Parameter(name = 'cdL3x1',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL31',
                   texname = '\\text{cdL3x1}')

cdL3x2 = Parameter(name = 'cdL3x2',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL32',
                   texname = '\\text{cdL3x2}')

cdL3x3 = Parameter(name = 'cdL3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'cdL33',
                   texname = '\\text{cdL3x3}')

I1a11 = Parameter(name = 'I1a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR1x1*yup',
                  texname = '\\text{I1a11}')

I1a12 = Parameter(name = 'I1a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR2x1*yc',
                  texname = '\\text{I1a12}')

I1a13 = Parameter(name = 'I1a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR3x1*yt',
                  texname = '\\text{I1a13}')

I1a21 = Parameter(name = 'I1a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR1x2*yup',
                  texname = '\\text{I1a21}')

I1a22 = Parameter(name = 'I1a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR2x2*yc',
                  texname = '\\text{I1a22}')

I1a23 = Parameter(name = 'I1a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR3x2*yt',
                  texname = '\\text{I1a23}')

I1a31 = Parameter(name = 'I1a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR1x3*yup',
                  texname = '\\text{I1a31}')

I1a32 = Parameter(name = 'I1a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR2x3*yc',
                  texname = '\\text{I1a32}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR3x3*yt',
                  texname = '\\text{I1a33}')

I2a11 = Parameter(name = 'I2a11',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR1x1*yup',
                  texname = '\\text{I2a11}')

I2a12 = Parameter(name = 'I2a12',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR1x2*yup',
                  texname = '\\text{I2a12}')

I2a13 = Parameter(name = 'I2a13',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR1x3*yup',
                  texname = '\\text{I2a13}')

I2a21 = Parameter(name = 'I2a21',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR2x1*yc',
                  texname = '\\text{I2a21}')

I2a22 = Parameter(name = 'I2a22',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR2x2*yc',
                  texname = '\\text{I2a22}')

I2a23 = Parameter(name = 'I2a23',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR2x3*yc',
                  texname = '\\text{I2a23}')

I2a31 = Parameter(name = 'I2a31',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR3x1*yt',
                  texname = '\\text{I2a31}')

I2a32 = Parameter(name = 'I2a32',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR3x2*yt',
                  texname = '\\text{I2a32}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'cuR3x3*yt',
                  texname = '\\text{I2a33}')

