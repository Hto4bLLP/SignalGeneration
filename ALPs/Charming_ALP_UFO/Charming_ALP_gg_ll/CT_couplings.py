# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 12.2.0 for Linux x86 (64-bit) (December 12, 2020)
# Date: Fri 2 Jul 2021 12:25:58


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



