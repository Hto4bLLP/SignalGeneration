#!/bin/bash

NOW=$(date +"%m-%d-%y-%H-%M-%S")

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup AthGeneration,21.6.90,here

mkdir submitDir.$1.$NOW
cp -r Charming_ALP_UFO submitDir.$1.$NOW
cd submitDir.$1.$NOW

Gen_tf.py --ecmEnergy=13000 --randomSeed=$1 --jobConfig=../100xxx/$1 --outputEVNTFile=$1.EVNT.root 
cp log.generate ../100xxx/$1/
cd ../
rm -f $1
ln -sf submitDir.$1.$NOW $1
