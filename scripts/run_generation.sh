#!/bin/bash

NOW=$(date +"%m-%d-%y-%H-%M-%S")

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup AthGeneration,21.6.66,here

mkdir submitDir.$1.$NOW
cp $2/*.* submitDir.$1.$NOW
cd submitDir.$1.$NOW

Gen_tf.py --ecmEnergy=13000 --randomSeed=$1 --jobConfig=../100xxx/$1 --outputEVNTFile=$1.EVNT.root --inputGeneratorFile=`ls -m *tar.gz.1 | xargs | sed 's/ /,/g' | sed 's/,,/,/g'`
cp log.generate ../100xxx/$1/
rm -f TXT*
cd ../
rm -f $1
ln -sf submitDir.$1.$NOW $1
