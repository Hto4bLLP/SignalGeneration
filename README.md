# Setup

Set up your environment
```
asetup AthGeneration,21.6,latest,here
```

# Running

Run a test using
```
Gen_tf.py --ecmEnergy=13000 --randomSeed=1 --jobConfig=100001 --outputEVNTFile=tmp.EVNT.root --maxEvents=5000
```
or use the provided script
```
source scripts/run_generation.sh 100001
```

# TRUTH3 Derivation

To convert the HEPMC format into a readable AOD, run
```
asetup AthDerivation,21.2,latest,here
Reco_tf.py Reco_tf.py --inputEVNTFile tmp.EVNT.root --outputDAODFile test.pool.root --reductionConf TRUTH3
```
or use the provided script
```
source scripts/run_derivation.sh 100001
```

# Validation plots

For validation, a [separate repository](https://gitlab.cern.ch/jburzyns/hzzd-truth-alg) exists
